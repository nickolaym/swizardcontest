import re
import operator

# pythonic idiom
def splitcases(s):
  ''' splits a multiline text with empty first line into separate strings '''
  return s.splitlines()[1:]

testcases = splitcases('''
sample_1() -> <<"(+ (* 4 4) (* 2 (- 7 5)) 1)">>.
sample_2() -> <<"10">>.
sample_3() -> <<"(* 10 (- 0 1))">>.
sample_4() -> <<"(- (+ 10 10) -5 0)">>.
sample_5() -> <<"(+ (- (* (+ (- (* 1))))))">>.
sample_6() -> <<"(* 2 (+ (- 10 9) (- 3 (* 2 1))) (+ (- 10 9) (- 3 (* 2 1))))">>.
sample_7() -> <<"(+ (* 2 1) (+ 8 8) (- (+ 4 3 2 1) (* 3 3) (* 2 2)) (* 5 7))">>.
sample_8() -> <<"(- (+ (+ 3 3) (- 3 3) (+ 3 3) (- 3 3)) (* 2 2))">>.
''')

def parse(s):
  ''' returns the identifier and the list-of-lists... of atoms '''
  # parse the erlang line: identifier and its value
  ident,value = re.match(r'^(.*)\s+->\s+<<"(.*)">>\.$', s).group(1,2)
  # translate the string value into python list
  regex = r'([+\-]?[0-9]+)|([(])|([)])|([+\-*])|\s+'
  def subber(m):
    if m.group(1): # number
      return m.group(1)+', '
    elif m.group(2):
      return '['
    elif m.group(3):
      return '], '
    elif m.group(4): # op
      return '"' + m.group(4) + '", '
    else:
      return ''
  pystr = re.sub(regex, subber, value)
  # let python parse it
  return ident, eval(pystr)[0]

# pythonic idiom
def unzip(list_of_tuples):
  ''' unzip list of same-arity tuples into a tuple of lists '''
  return zip(*list_of_tuples)

# table of operators and their costs
ops = {
  '+' : (operator.add, 2),
  '-' : (operator.sub, 3),
  '*' : (operator.mul, 10),
}

def calc(expr):
  ''' returns arithmetic result and cost of an expression '''
  if type(expr)==int:
    return expr, 0
  assert type(expr)==list
  op,cost = ops[expr[0]]
  args, costs = unzip([calc(arg) for arg in expr[1:]])
  return reduce(op, args), cost + max(costs)

for s in testcases:
  ident, expr = parse(s)
  res,time = calc(expr)
  print ident, '==>', res, '(delay', time, 's)'
